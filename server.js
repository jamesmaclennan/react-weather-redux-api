'use strict'

const express = require('express');
const cors = require('cors')
const fetch = require('node-fetch');
const MongoClient = require('mongodb');
const assert = require('assert');

const app = express();

const config = {
    mongoDBUrl: 'mongodb://mongodb/world',
    darkSkyAPIKey: '2486ab66ccdd11f08cf943897609fb90',
    googleAPIKey: 'AIzaSyBcALD_qI9cJshprdxbvPW9SS9hNaBnvNg'
}

//mongoimport --db dbNamemongo --collection collectionName --file fileName.json --jsonArray
//mongoimport --db world --collection locations --file world.json --jsonArray

app.options('/search/:terms', cors());
app.options('/geocode/:latitude/:longitude', cors());


// set headers for all responses made by Express
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.header('Content-Type', 'application/json');
    next();
})

// Receive lat & long coordinates from front-end, fetch them from
// darkSky API and forward them back to front-end.

let apiFetch = {
    fetchPromise: (url, latitude, longitude) => {
        let fetchReturn = new Promise((resolve, reject) => {
            fetch(url)
            .then(response =>
                response.json().then(data => ({
                    data: data,
                    status: response.status
                })
            )
            .then(response =>  {
                resolve(response.data)
            }));
        });
        return fetchReturn
    },
    // get weather data and return promised data
    darkSky: (latitude, longitude) => {
        let url = `https://api.darksky.net/forecast/${config.darkSkyAPIKey}/${latitude},${longitude}?units=ca&exclude=[minutely,alerts,flags]`;
        return apiFetch.fetchPromise(url, latitude, longitude);
    },
    // get city name from lat/long coords
    googleGeocode: (latitude, longitude) => {
        let url = `https://maps.googleapis.com/maps/api/geocode/json?latlng=${latitude},${longitude}&key=${config.googleAPIKey}`;
        return apiFetch.fetchPromise(url, latitude, longitude);
    }
}

app.get('/location/:latitude/:longitude', (req, res) => {

	let locationObject = {};

    let createLocationObject = new Promise((resolve, reject) => {

        // wait for the weather data to be added.
        let weatherPromise = new Promise((resolve, reject) => {
            apiFetch.darkSky(req.params.latitude, req.params.longitude).then(data => {
                locationObject.weather = data;
                resolve(true);
            })

        })

        // wait for the name data, parse it into a simple object with city and state names and add it.
        let namePromise = new Promise((resolve, reject) => {
            apiFetch.googleGeocode(req.params.latitude, req.params.longitude).then(data => {
                locationObject.name = data;

                let locationName = {};
                data.results.map((result) => {
                    result.address_components.map((component) => {

                        // get the city name
                        if ((component.types.indexOf('locality') !== -1) && (result.address_components[0].types.indexOf('political') !== -1)) {
                            locationName.city = component;
                        }

                        // get the state name
                        if ((component.types.indexOf('administrative_area_level_1') !== -1) && (result.address_components[0].types.indexOf('political') !== -1)) {
                            locationName.state = component;
                        }
                    })
                })
                locationObject.name = locationName;

                resolve(true);
            })
        })

        // wait for both the name and weather data to be dumped into the locationObject
        // then send response.
        Promise.all([weatherPromise, namePromise]).then(values => {
            res.send(locationObject);
        })

    })
});

// receive type-ahead data from location search bar on front-end
// and return a matching list of location names
app.get('/search/:terms', cors(), (req, res) => {
    MongoClient.connect(config.mongoDBUrl, function(err, db) {
        var locationsCollection = db.collection('locations');
        locationsCollection.find({city: new RegExp(req.params.terms, "i")}).limit(10).toArray(function(err, data) {
            db.close();
            res.json(data);
        })
    })
});

//reverse geocode lookup from google's API to return a city name from
//lat and long values. Needed for browser's location coordinates.
app.get('/geocode/:latitude/:longitude', cors(), (req, res) => {
    apiFetch.googleGeocode(req.params.latitude, req.params.longitude)
})

app.listen(80);
